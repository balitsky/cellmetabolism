using System;
using System.Text; 
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Net.Mail;
using System.Net;
using Renci.SshNet;
using System.IO;

namespace Cell
{
    

    
    public static class Field
    {
        public const int type = 0; 
        public const int screenSizeX = 1024;
        public const int screenSizeY = 768;
        public const int cellSizeX = 16;
        public const int cellSizeY = 16;
        public const int gridSizeX = screenSizeX / cellSizeX;
        public const int gridSizeY = screenSizeY / cellSizeY;
        public const int organelleTypeNumber = 10;
        public const int resourceTypeNumber = 14;
        public const int organelleMaxNumber = 75;
        public struct gridCell
        {
            public int content;
            public Organelle org;
        }
        public static gridCell[,] grid = new gridCell[gridSizeX, gridSizeY];

        public struct Reserves
        {
          //  public int targetType, targetNumber;
            public int[] resources;
        }
        public static Reserves[,] actualReserves = new Reserves[organelleTypeNumber, organelleMaxNumber];
        public static Reserves[,] expectedReserves = new Reserves[organelleTypeNumber, organelleMaxNumber];
        public static Reserves[,] needs = new Reserves[organelleTypeNumber, organelleMaxNumber];
        public static Reserves[,] surpluses = new Reserves[organelleTypeNumber, organelleMaxNumber];
        public static int orgFullNumber = 0;
        public static int[] orgPartialNumber = new int[organelleTypeNumber];
        public static Organelle[,] org = new Organelle[organelleTypeNumber, organelleMaxNumber];

        static Field()
        {
            for (int i = 0; i < organelleTypeNumber; i++)
            {
                orgPartialNumber[i] = 0;
            }
            for (int i  = 0; i < organelleTypeNumber; i++)
            {
                for (int j = 0; j < organelleMaxNumber; j++)
                {
                    expectedReserves[i, j].resources = new int[resourceTypeNumber];
                    Array.Clear(expectedReserves[i, j].resources, 0, expectedReserves[i, j].resources.Length);
                    needs[i, j].resources = new int[resourceTypeNumber];
                    Array.Clear(needs[i, j].resources, 0, needs[i, j].resources.Length);
                    surpluses[i, j].resources = new int[resourceTypeNumber];
                    Array.Clear(surpluses[i, j].resources, 0, surpluses[i, j].resources.Length);
                }
            }
            for (int i = 0; i < Field.gridSizeX; i++)
            {
                for (int j = 0; j < Field.gridSizeY; j++)
                {
                    grid[i, j].content = Field.type;
                }
            }
        }
        
    }

    public abstract class Organelle
    {
        public struct Part
        {
            public int X, Y;

        }
        public int type;
        public int num;
        public Part[] Body;
        public int partNumber = 0;
        public int energy = 20;
        protected const int maxtime = 65536;
        protected int time = 0;
        protected int busy = 0;
        public void putPart(int X, int Y)
        {
            Body[partNumber].X = X;
            Body[partNumber].Y = Y;
            partNumber++;
        }
        protected void ConsumeATP()
        {
            if (resources[(int)Resources.ATP] > 0)
            {
                resources[(int)Resources.ATP]--;
                energy += 160;
                resources[(int)Resources.phosphate] += 3;
                resources[(int)Resources.nucleobase] += 1;
                resources[(int)Resources.fattyacid] += 1;
            }
        }
        protected void ConsumeGlucose()
        {
            if ((resources[(int)Resources.monosaccharide] > 0) && (resources[(int)Resources.oxygen] > 0))
            {
                resources[(int)Resources.monosaccharide]--;
                resources[(int)Resources.oxygen]--;
                resources[(int)Resources.carbondioxide]++;
                energy += 40;
            }
        }
        public Part Corner;
        public Vector2 Center;
        public enum Resources
        {
            ATP, oxygen, aminoacid, protein,
            nucleobase, RNA, DNA, carbohydrate, monosaccharide, lipid, fattyacid, phosphate, carbondioxide, micronutrient
        }
        public enum Types
        {
            Empty = 0, Membrane = 1, Cytoplasm = 2, Nucleus = 3, Mitochondrion = 4, Pore = 5, EndoplasmicReticulum = 6, 
            Lysosome = 7, Ribosome = 8, GolgiApparatus = 9
        }
        public int[] resources = new int[Field.resourceTypeNumber];
        public int[] importance = new int[Field.resourceTypeNumber];
      //  public virtual void InitResources() { }
        public virtual void Update() { }
        public virtual void CalculateNeeds() 
        {
            for (int i = 0; i < Field.resourceTypeNumber; i++)
            {
                switch (importance[i])
                {
                    case 0:
                        Field.needs[type, num].resources[i] = -10000;
                        Field.surpluses[type, num].resources[i] = 1000;
                        break;
                    case 1:
                        Field.needs[type, num].resources[i] = 10 * (5 - 2 * Field.actualReserves[type, num].resources[i] - Field.expectedReserves[type, num].resources[i]);
                        Field.surpluses[type, num].resources[i] = 20 * (-3 + Field.actualReserves[type, num].resources[i]);
                        break;
                    case 2:
                        Field.needs[type, num].resources[i] = 6 * (10 - 2 * Field.actualReserves[type, num].resources[i] - Field.expectedReserves[type, num].resources[i]);
                        Field.surpluses[type, num].resources[i] = 10 * (-6 + Field.actualReserves[type, num].resources[i]);
                        break;
                    case 3:
                        Field.needs[type, num].resources[i] = 4 * (15 - 2 * Field.actualReserves[type, num].resources[i] - Field.expectedReserves[type, num].resources[i]);
                        Field.surpluses[type, num].resources[i] = 6 * (-10 + Field.actualReserves[type, num].resources[i]);
                        break;
                    case 4:
                        Field.needs[type, num].resources[i] = 3 * (25 - 2 * Field.actualReserves[type, num].resources[i] - Field.expectedReserves[type, num].resources[i]);
                        Field.surpluses[type, num].resources[i] = 4 * (-15 + Field.actualReserves[type, num].resources[i]);
                        break;
                    case 5:
                        Field.needs[type, num].resources[i] = 2 * (35 - 2 * Field.actualReserves[type, num].resources[i] - Field.expectedReserves[type, num].resources[i]);
                        Field.surpluses[type, num].resources[i] = 3 * (-25 + Field.actualReserves[type, num].resources[i]);
                        break;
                        

                    /*default:
                        if ((2 * Field.actualReserves[type, num].resources[i] + Field.expectedReserves[type, num].resources[i]) <= 4)
                        {
                            Field.needs[type, num].resources[i] = 50;
                        }
                        else
                        {
                            if ((2 * Field.actualReserves[type, num].resources[i] + Field.expectedReserves[type, num].resources[i]) <= 15)
                            {
                                Field.needs[type, num].resources[i] = 75 - 5 * (2 * Field.actualReserves[type, num].resources[i] + Field.expectedReserves[type, num].resources[(int)Organelle.Resources.ATP]);
                            }
                            else
                            {
                                Field.needs[type, num].resources[i] = 0;
                            }
                        }
                        
                        if (Field.actualReserves[type, num].resources[i] <= 2)
                        {
                            Field.surpluses[type, num].resources[i] = 0;
                        }
                        else
                        {
                            Field.surpluses[type, num].resources[i] =
                                Field.actualReserves[type, num].resources[(int)Organelle.Resources.ATP] - 2;
                        }
                        break;*/
                }
                if (Field.actualReserves[type, num].resources[i] == 0)
                {
                    Field.surpluses[type, num].resources[i] = -10000;
                }
            }
        }
    }

    public class Membrane : Organelle
    {
        //public const int type = 1;
        private Random r = new Random();
 
        private void RightDown(ref int x, ref int y, int q, int t)
        {
            if (r.Next((int)Math.Floor(Math.Sqrt(t))) >= (int)Math.Floor(Math.Sqrt(q)))
            {
                y++;
            }
            else
            {
                x++;
            }
        }
        private void DownLeft(ref int x, ref int y, int q, int t)
        {
            if (r.Next((int)Math.Floor(Math.Sqrt(t))) >= (int)Math.Floor(Math.Sqrt(q)))
            {
                x--;
            }
            else
            {
                y++;
            }
        }
        private void LeftUp(ref int x, ref int y, int q, int t)
        {
            if (r.Next((int)Math.Floor(Math.Sqrt(t))) >= (int)Math.Floor(Math.Sqrt(q)))
            {
                y--;
            }
            else
            {
                x--;
            }
        }
        private void UpRight(ref int x, ref int y, int q, int t)
        {
            if (r.Next((int)Math.Floor(Math.Sqrt(t))) >= (int)Math.Floor(Math.Sqrt(q)))
            {
                x++;
            }
            else
            {
                y--;
            }
        }
              
        public Membrane(int wid, int hei)
        {
            type = 1;
            //Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            Body = new Part[2 * wid + 2 * hei - 2];
            int minX = (Field.gridSizeX - wid) / 2;
            int maxX = minX + wid - 1;
            int minY = (Field.gridSizeY - hei) / 2;
            int maxY = minY + hei - 1;

            int t = Math.Min(wid / 2, hei / 2);

            int x = maxX - t;
            int y = minY;


            while (x < maxX)
            {
                this.putPart(x, y);
                Field.grid[x, y].content = this.type;
                Field.grid[x, y].org = this;
                if (y < maxY - t - 1)
                {
                    RightDown(ref x, ref y, maxX - x, t);
                }
                else
                {
                    x++;
                }
            }
            while (y < maxY)
            {
                this.putPart(x, y);
                Field.grid[x, y].content = this.type;
                Field.grid[x, y].org = this;
                if (x > minX + t + 1)
                {
                    DownLeft(ref x, ref y, maxY - y, t);
                }
                else
                {
                    y++;
                }
            }
            while (x > minX)
            {
                this.putPart(x, y);
                Field.grid[x, y].content = this.type;
                Field.grid[x, y].org = this;
                if (y > minY + t + 1)
                {
                    LeftUp(ref x, ref y, x - minX, t);
                }
                else
                {
                    x--;
                }
            }
            while (y > minY)
            {
                this.putPart(x, y);
                Field.grid[x, y].content = this.type;
                Field.grid[x, y].org = this;
                if (x < maxX - t - 1)
                {
                    UpRight(ref x, ref y, y - minY, t);
                }
                else
                {
                    y--;
                }
            }
            for (int i = x; i < maxX - t; i++)
            {
                this.putPart(i, minY);
                Field.grid[i, minY].content = this.type;
                Field.grid[i, minY].org = this;
            }

        }

        private int nextPart(int p)
        {
            return (p + 1) % partNumber;
        }
        private int prevPart(int p)
        {
            if (p == 0)
                return partNumber - 1;
            return p - 1;
        }

        private int prevOrient(int p)
        {
            switch ((Body[p].X - Body[prevPart(p)].X) * 2 + Body[p].Y - Body[prevPart(p)].Y + 2)
            {
                case 0: return 2;
                case 1: return 3;
                case 4: return 0;
            }
            return 1;
        }
        private int nextOrient(int p)
        {
            switch ((Body[p].X - Body[nextPart(p)].X) * 2 + Body[p].Y - Body[nextPart(p)].Y + 2)
            {
                case 0: return 2;
                case 1: return 3;
                case 4: return 0;
            }
            return 1;
        }
        public int PartOrientation(int p)
        {
            return prevOrient(p) * 4 + nextOrient(p);
        }

    }

    public class Cytoplasm : Organelle
    {
       // public const int type = 2;
        
        private void fillDFS(int x, int y)
        {
            if (Field.grid[x, y].content == Field.type)
            {
                Field.grid[x, y].content = this.type;
                Field.grid[x, y].org = this;
                Array.Resize(ref Body, Body.Length + 1);
                putPart(x, y);
                if (x > 0)
                {
                    fillDFS(x - 1, y);
                }
                if (x < Field.gridSizeX - 1)
                {
                    fillDFS(x + 1, y);
                }
                if (y > 0)
                {
                    fillDFS(x, y - 1);
                }
                if (y < Field.gridSizeY - 1)
                {
                    fillDFS(x, y + 1);
                }
            }
        }
        public Cytoplasm()
        {
            type = 2;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            Body = new Part[0];
            fillDFS(Field.gridSizeX / 2, Field.gridSizeY / 2);

        }

    }
    
    public class Nucleus : Organelle
    {
        private Random r = new Random();        

        private int[,] Mask  =
        {
            {0,1,1,1,1,1,0},
            {1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1},
            {0,1,1,1,1,1,0}
        };
        private bool checkMask(int x, int y)
        {
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 1) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public Nucleus(int wid, int hei)
        {
            type = 3;
            Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            bool success = false;
            int x,y;
            int counter = 0;
            Body = new Part[0];
            do
            {
                int minX = (Field.gridSizeX - wid) / 2;
                int minY = (Field.gridSizeY - hei) / 2;
                x = 1 + minX + r.Next(wid - Mask.GetLength(1));
                y = 1 + minY + r.Next(hei - Mask.GetLength(0));
                if (checkMask(x, y))
                {
                    for (int i = 0; i < Mask.GetLength(1); i++)
                    {
                        for (int j = 0; j < Mask.GetLength(0); j++)
                        {
                            if (Mask[j, i] == 1) 
                            {
                                Array.Resize(ref Body, Body.Length + 1);
                                putPart(x+i, y+i);
                                Field.grid[x + i, y + j].content = this.type;
                                Field.grid[x + i, y + j].org = this;
                            }
                        }
                    }
                    Corner.X = x;
                    Corner.Y = y;
                    Center.X = (Corner.X * Field.cellSizeX + (Mask.GetLength(1) * Field.cellSizeX) / 2);
                    Center.Y = (Corner.Y * Field.cellSizeY + (Mask.GetLength(0) * Field.cellSizeY) / 2);
                    success = true;
                }
                counter++;
            }
            while ((!success) && (counter < 1000000));

            if (counter == 1000000)
            {
                ///////////
                //// error!!!!!!!!!!!!!!!!!!!! �� ������ ����
                ///////////
            }

            InitResources();
            

        }
        private void InitResources()
        {
            energy += 100;
            resources[(int)Resources.ATP] = 10;
            resources[(int)Resources.oxygen] = 3;
            resources[(int)Resources.aminoacid] = 0;
            resources[(int)Resources.protein] = 0;
            resources[(int)Resources.nucleobase] = 0;
            resources[(int)Resources.RNA] = 4;
            resources[(int)Resources.DNA] = 1;
            resources[(int)Resources.carbohydrate] = 0;
            resources[(int)Resources.monosaccharide] = 5;
            resources[(int)Resources.lipid] = 0;
            resources[(int)Resources.fattyacid] = 0;
            resources[(int)Resources.phosphate] = 0;
            resources[(int)Resources.carbondioxide] = 0;
            resources[(int)Resources.micronutrient] = 0;
            importance[(int)Resources.ATP] = 3;
            importance[(int)Resources.oxygen] = 1;
            importance[(int)Resources.aminoacid] = 5;
            importance[(int)Resources.protein] = 0;
            importance[(int)Resources.nucleobase] = 5;
            importance[(int)Resources.RNA] = 0;
            importance[(int)Resources.DNA] = 0;
            importance[(int)Resources.carbohydrate] = 0;
            importance[(int)Resources.monosaccharide] = 4;
            importance[(int)Resources.lipid] = 0;
            importance[(int)Resources.fattyacid] = 0;
            importance[(int)Resources.phosphate] = 4;
            importance[(int)Resources.carbondioxide] = 1;
            importance[(int)Resources.micronutrient] = 1;
            
        }

        private enum Actions { nothing, genRNA };
        private Actions action = Actions.nothing;
        private void ChooseBehavior()
        {
            if (r.Next(512) == 0)
            {
                if ((resources[(int)Resources.nucleobase] >= 12) && (resources[(int)Resources.phosphate] >= 12)
                    && (resources[(int)Resources.monosaccharide] >= 12))
                {
                    busy = 12000;
                    action = Actions.genRNA;
                    resources[(int)Resources.nucleobase] -= 12;
                    resources[(int)Resources.phosphate] -= 12;
                    resources[(int)Resources.monosaccharide] -= 12;
                }
            }
        }

        private void FinishAction()
        {
            switch (action)
            {
                case Actions.genRNA:
                    action = Actions.nothing;
                    resources[(int)Resources.RNA] += 1;
                    break;
            }

        }
        public override void Update()
        {
            time++;
            if (time == maxtime)
            {
                time = 0;
            }


            if (energy > 0)
            {
                if (busy > 0)
                {
                    busy--;
                    switch (action)
                    {
                        case Actions.genRNA:
                            if (time % 64 == 0)
                            {
                                energy--;
                            }
                            break;
                    }
                    if (busy == 0)
                    {
                        FinishAction();
                    }
                }
                else
                    ChooseBehavior();
            }


            if (time % 64 == 0)
            {
                if (energy > 0)
                {
                    energy--;
                }
            }
            if (energy == 0)
            {
                if (r.Next(4) > 0)
                {
                    ConsumeATP();
                    if (energy == 0)
                    {
                        ConsumeGlucose();
                    }
                }
                else
                {
                    ConsumeGlucose();
                    if (energy == 0)
                    {
                        ConsumeATP();
                    }    
                }
             
            }
            if (energy == 0)
            {
                ////////////////////
                //// ��������� �������
                //////////////////
            }

        }
    }

    public class Mitochondrion : Organelle
    {
        private Random r = new Random();        
        private int[,] Mask  =
        {
            {0,1,1,1,1,1,1,0},
            {1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1},
            {0,1,1,1,1,1,1,0}
        };

        private bool checkMask(int x, int y)
        {
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 1) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public Mitochondrion(int wid, int hei)
        {
            type = 4;
            Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            bool success = false;
            int x,y;
            int counter = 0;
            Body = new Part[0];
            do
            {
                int minX = (Field.gridSizeX - wid) / 2;
                int minY = (Field.gridSizeY - hei) / 2;
                x = 1 + minX + r.Next(wid - Mask.GetLength(1));
                y = 1 + minY + r.Next(hei - Mask.GetLength(0));
                if (checkMask(x, y))
                {
                    for (int i = 0; i < Mask.GetLength(1); i++)
                    {
                        for (int j = 0; j < Mask.GetLength(0); j++)
                        {
                            if (Mask[j, i] == 1) 
                            {
                                Array.Resize(ref Body, Body.Length + 1);
                                putPart(x+i, y+i);
                                Field.grid[x + i, y + j].content = this.type;
                                Field.grid[x + i, y + j].org = this;
                            }
                        }
                    }
                    Corner.X = x;
                    Corner.Y = y;
                    Center.X = (Corner.X * Field.cellSizeX + (Mask.GetLength(1) * Field.cellSizeX) / 2);
                    Center.Y = (Corner.Y * Field.cellSizeY + (Mask.GetLength(0) * Field.cellSizeY) / 2);
                    success = true;
                }
                counter++;
            }
            while ((!success) && (counter < 1000000));

            if (counter == 1000000)
            {
                ///////////
                //// error!!!!!!!!!!!!!!!!!!!! �� ������
                ///////////
            }

            InitResources();


        }
        private void InitResources()
        {
            energy += 100;
            resources[(int)Resources.ATP] = 5;
            resources[(int)Resources.oxygen] = 5;
            resources[(int)Resources.aminoacid] = 0;
            resources[(int)Resources.protein] = 0;
            resources[(int)Resources.nucleobase] = 5;
            resources[(int)Resources.RNA] = 0;
            resources[(int)Resources.DNA] = 1;
            resources[(int)Resources.carbohydrate] = 0;
            resources[(int)Resources.monosaccharide] = 5;
            resources[(int)Resources.lipid] = 0;
            resources[(int)Resources.fattyacid] = 0;
            resources[(int)Resources.phosphate] = 0;
            resources[(int)Resources.carbondioxide] = 0;
            resources[(int)Resources.micronutrient] = 0;
            importance[(int)Resources.ATP] = 1;
            importance[(int)Resources.oxygen] = 3;
            importance[(int)Resources.aminoacid] = 2;
            importance[(int)Resources.protein] = 0;
            importance[(int)Resources.nucleobase] = 3;
            importance[(int)Resources.RNA] = 0;
            importance[(int)Resources.DNA] = 0;
            importance[(int)Resources.carbohydrate] = 0;
            importance[(int)Resources.monosaccharide] = 4;
            importance[(int)Resources.lipid] = 0;
            importance[(int)Resources.fattyacid] = 4;
            importance[(int)Resources.phosphate] = 5;
            importance[(int)Resources.carbondioxide] = 1;
            importance[(int)Resources.micronutrient] = 0;
            
        }
        
        private enum Actions {nothing, genATP, genNucleobase};
        private Actions action = Actions.nothing;
        private void ChooseBehavior()
        {
            switch (r.Next(256))
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    if ((resources[(int)Resources.oxygen] >= 2) && (resources[(int)Resources.nucleobase] >= 1) && (resources[(int)Resources.phosphate] >= 3)
                        && (resources[(int)Resources.monosaccharide] >= 1) && (resources[(int)Resources.fattyacid] >= 1))
                    {
                        busy = 1200;
                        action = Actions.genATP;
                        resources[(int)Resources.oxygen] -= 2;
                        resources[(int)Resources.nucleobase] -= 1;
                        resources[(int)Resources.phosphate] -= 3;
                        resources[(int)Resources.fattyacid] -= 1;
                        resources[(int)Resources.monosaccharide] -= 1;
                    }
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                    if ((resources[(int)Resources.oxygen] >= 1) && (resources[(int)Resources.monosaccharide] >= 1)
                        && (resources[(int)Resources.aminoacid] >= 1) && (resources[(int)Resources.fattyacid] >= 1))
                    {
                        busy = 600;
                        action = Actions.genNucleobase;
                        resources[(int)Resources.oxygen] -= 1;
                        resources[(int)Resources.fattyacid] -= 1;
                        resources[(int)Resources.aminoacid] -= 1;
                        resources[(int)Resources.monosaccharide] -= 1;
                    }
                    break;
            }
        }
       
        private void FinishAction()
        {
            switch (action)
            {
                case Actions.genATP:
                    action = Actions.nothing;
                    resources[(int)Resources.ATP] += 1;
                    resources[(int)Resources.carbondioxide] += 2;
                    break;
                case Actions.genNucleobase:
                    action = Actions.nothing;
                    resources[(int)Resources.nucleobase] += 1;
                    resources[(int)Resources.carbondioxide] += 2;
                    break;
            }

        }
        public override void Update()
        {
            time++;
            if (time == maxtime)
            {
                time = 0;
            }

            if (energy > 0)
            {
                if (busy > 0)
                {
                    busy--;
                    switch (action)
                    {
                        case Actions.genATP:
                        case Actions.genNucleobase:
                            if (time % 32 == 0) 
                            {
                                energy--;
                            }
                            break;
                    }
                    if (busy == 0)
                    {
                        FinishAction();
                    }
                }
                else
                    ChooseBehavior();
            }

            if (time % 64 == 0)
            {
                if (energy > 0)
                {
                    energy--;
                }
            }

            if (energy == 0)
            {
                ConsumeGlucose();
                if (energy == 0)
                {
                    ConsumeATP();
                }
                if (energy <= 0)
                {
                    ////////////////////
                    //// ��������� �������
                    //////////////////

                }
            }
        }
    }

    public class Pore : Organelle
    {
        private Random r = new Random();
        private int[,] Mask =
        {
            {1}
        };

        public Pore()
        {
            type = 5;
            Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            bool success = false;
            int x, y, n;
            int counter = 0;
            Body = new Part[1];
            do
            {
                n = r.Next(Field.org[(int)Organelle.Types.Membrane, 0].partNumber);
                x = Field.org[(int)Organelle.Types.Membrane, 0].Body[n].X;
                y = Field.org[(int)Organelle.Types.Membrane, 0].Body[n].Y;
                if (((Field.grid[x, y].content == (int)Organelle.Types.Membrane) && (Field.grid[x, y - 1].content == (int)Organelle.Types.Membrane) && (Field.grid[x, y + 1].content == (int)Organelle.Types.Membrane))
                    || ((Field.grid[x, y].content == (int)Organelle.Types.Membrane) && (Field.grid[x - 1, y].content == (int)Organelle.Types.Membrane) && (Field.grid[x + 1, y].content == (int)Organelle.Types.Membrane)))
                {
                    putPart(x, y);
                    Field.grid[x, y].content = this.type;
                    Field.grid[x, y].org = this;
                    Corner.X = x;
                    Corner.Y = y;
                    Center.X = (Corner.X * Field.cellSizeX + (Mask.GetLength(1) * Field.cellSizeX) / 2);
                    Center.Y = (Corner.Y * Field.cellSizeY + (Mask.GetLength(0) * Field.cellSizeY) / 2);
                    success = true;
                }
                counter++;
            }
            while ((!success) && (counter < 1000000));

            if (counter == 1000000)
            {
                ///////////
                //// error!!!!!!!!!!!!!!!!!!!! �� ������
                ///////////
            }

            InitResources();


        }
        private void InitResources()
        {
            resources[(int)Resources.ATP] = 0;
            resources[(int)Resources.oxygen] = 0;
            resources[(int)Resources.aminoacid] = 0;
            resources[(int)Resources.protein] = 0;
            resources[(int)Resources.nucleobase] = 0;
            resources[(int)Resources.RNA] = 0;
            resources[(int)Resources.DNA] = 0;
            resources[(int)Resources.carbohydrate] = 0;
            resources[(int)Resources.monosaccharide] = 0;
            resources[(int)Resources.lipid] = 0;
            resources[(int)Resources.fattyacid] = 0;
            resources[(int)Resources.phosphate] = 0;
            resources[(int)Resources.carbondioxide] = 0;
            resources[(int)Resources.micronutrient] = 0;
            importance[(int)Resources.ATP] = 0;
            importance[(int)Resources.oxygen] = 0;
            importance[(int)Resources.aminoacid] = 0;
            importance[(int)Resources.protein] = 0;
            importance[(int)Resources.nucleobase] = 0;
            importance[(int)Resources.RNA] = 0;
            importance[(int)Resources.DNA] = 0;
            importance[(int)Resources.carbohydrate] = 0;
            importance[(int)Resources.monosaccharide] = 0;
            importance[(int)Resources.lipid] = 0;
            importance[(int)Resources.fattyacid] = 0;
            importance[(int)Resources.phosphate] = 0;
            importance[(int)Resources.carbondioxide] = 5;
            importance[(int)Resources.micronutrient] = 0;
        }

        public override void Update()
        {
            time++;
            if (time == maxtime)
            {
                time = 0;
            }

            if (time % 256 == 0)
            {
                switch (r.Next(51))
                {
                    case 0:
                    case 1:
                    case 2:
                        Transport.OrderResource((int)Organelle.Resources.oxygen, 6, this.num);
                        break;
                    case 3:
                    case 4:
                        Transport.OrderResource((int)Organelle.Resources.aminoacid, 2, this.num);
                        break;
                    case 5:
                    case 6:
                        Transport.OrderResource((int)Organelle.Resources.phosphate, 4, this.num);
                        break;
                    case 7:
                        Transport.OrderResource((int)Organelle.Resources.micronutrient, 1, this.num);
                        break;
                    case 8:
                    case 9:
                    case 10:
                        Transport.OrderResource((int)Organelle.Resources.fattyacid, 2, this.num);
                        break;
                    case 11:
                    case 12:
                        Transport.OrderResource((int)Organelle.Resources.carbohydrate, 1, this.num);
                        break;
                }
                if (resources[(int)Organelle.Resources.carbondioxide] > 0)
                {
                    Transport.Utilize(this, (int)Organelle.Resources.carbondioxide, 1);
                }
            }
        }

    }

    public class EndoplasmicReticulum : Organelle
    {
        private Random r = new Random();
        private int[,] Mask =
        {
            {2,1,1,1,1,1,2},
            {1,1,1,1,1,1,1},
            {2,1,1,1,1,1,2},
            {0,2,1,1,1,2,0}
        };

        private bool checkMask(int x, int y)
        {
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 1) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm))
                    {
                        //if (!(((i == 0) || (i == Mask.GetLength(1) - 1)) && ((j == 0) || (j == Mask.GetLength(0) - 1))
                        //    && ((Field.grid[x + i, y + j].content == (int)Organelle.Types.Nucleus) || (Field.grid[x + i, y + j].content == (int)Organelle.Types.EndoplasmicReticulum))))
                        return false;
                    }
                    if ((Mask[j, i] == 2) && ((Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm) && 
                        (Field.grid[x + i, y + j].content != (int)Organelle.Types.Nucleus) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.EndoplasmicReticulum)))
                    {
                        return false;
                    }
                }
            }
            //bool b = false;
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 2) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public EndoplasmicReticulum(int wid, int hei)
        {
            type = 6;
            Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            bool success = false;
            int x, y;
            int counter = 0;
            Body = new Part[0];
            do
            {
                int minX = (Field.gridSizeX - wid) / 2;
                int minY = (Field.gridSizeY - hei) / 2;
                x = 1 + minX + r.Next(wid - Mask.GetLength(1));
                y = 1 + minY + r.Next(hei - Mask.GetLength(0));
                if (checkMask(x, y))
                {
                    for (int i = 0; i < Mask.GetLength(1); i++)
                    {
                        for (int j = 0; j < Mask.GetLength(0); j++)
                        {
                            if ((Mask[j, i] > 0) && (Field.grid[x + i, y + j].content == (int)Organelle.Types.Cytoplasm))
                            {
                                Array.Resize(ref Body, Body.Length + 1);
                                putPart(x + i, y + i);
                                Field.grid[x + i, y + j].content = this.type;
                                Field.grid[x + i, y + j].org = this;
                            }
                        }
                    }
                    Corner.X = x;
                    Corner.Y = y;
                    Center.X = (Corner.X * Field.cellSizeX + (Mask.GetLength(1) * Field.cellSizeX) / 2);
                    Center.Y = (Corner.Y * Field.cellSizeY + (Mask.GetLength(0) * Field.cellSizeY) / 2);
                    success = true;
                }
                counter++;
            }
            while ((!success) && (counter < 1000000));

            if (counter == 1000000)
            {

                ///////////
                //// error!!!!!!!!!!!!!!!!!!!! �� ������
                ///////////
            }

            InitResources();


        }
        private void InitResources()
        {
            energy += 80;
            resources[(int)Resources.ATP] = 6;
            resources[(int)Resources.oxygen] = 3;
            resources[(int)Resources.aminoacid] = 0;
            resources[(int)Resources.protein] = 0;
            resources[(int)Resources.nucleobase] = 0;
            resources[(int)Resources.RNA] = 0;
            resources[(int)Resources.DNA] = 0;
            resources[(int)Resources.carbohydrate] = 0;
            resources[(int)Resources.monosaccharide] = 3;
            resources[(int)Resources.lipid] = 0;
            resources[(int)Resources.fattyacid] = 0;
            resources[(int)Resources.phosphate] = 0;
            resources[(int)Resources.carbondioxide] = 0;
            resources[(int)Resources.micronutrient] = 0;
            importance[(int)Resources.ATP] = 3;
            importance[(int)Resources.oxygen] = 1;
            importance[(int)Resources.aminoacid] = 0;
            importance[(int)Resources.protein] = 1;
            importance[(int)Resources.nucleobase] = 0;
            importance[(int)Resources.RNA] = 0;
            importance[(int)Resources.DNA] = 0;
            importance[(int)Resources.carbohydrate] = 1;
            importance[(int)Resources.monosaccharide] = 1;
            importance[(int)Resources.lipid] = 1;
            importance[(int)Resources.fattyacid] = 3;
            importance[(int)Resources.phosphate] = 1;
            importance[(int)Resources.carbondioxide] = 1;
            importance[(int)Resources.micronutrient] = 1;
        }

        private enum Actions { nothing, genLipid };
        private Actions action = Actions.nothing;
        private void ChooseBehavior()
        {
            if (r.Next(512) == 0)
            {
                if ((resources[(int)Resources.fattyacid] >= 9) && (resources[(int)Resources.micronutrient] >= 1))
                {
                    busy = 25000;
                    action = Actions.genLipid;
                    resources[(int)Resources.fattyacid] -= 9;
                    resources[(int)Resources.micronutrient] -= 1;
                }
            }
        }

        private void FinishAction()
        {
            switch (action)
            {
                case Actions.genLipid:
                    action = Actions.nothing;
                    resources[(int)Resources.lipid] += 1;
                    break;
            }

        }
        public override void Update()
        {
            time++;
            if (time == maxtime)
            {
                time = 0;
            }


            if (energy > 0)
            {
                if (busy > 0)
                {
                    busy--;
                    switch (action)
                    {
                        case Actions.genLipid:
                            if (time % 128 == 0)
                            {
                                energy--;
                            }
                            break;
                    }
                    if (busy == 0)
                    {
                        FinishAction();
                    }
                }
                else
                    ChooseBehavior();
            }


            if (time % 64 == 0)
            {
                if (energy > 0)
                {
                    energy--;
                }
            }
            if (energy == 0)
            {
                if (r.Next(4) > 0)
                {
                    ConsumeATP();
                    if (energy == 0)
                    {
                        ConsumeGlucose();
                    }
                }
                else
                {
                    ConsumeGlucose();
                    if (energy == 0)
                    {
                        ConsumeATP();
                    }
                }

            }
            if (energy == 0)
            {
                ////////////////////
                //// ��������� �������
                //////////////////
            }
        }
    }
    
    public class Lysosome : Organelle
    {
        private Random r = new Random();
        private int[,] Mask =
        {
            {1,1},
            {1,1}
        };

        private bool checkMask(int x, int y)
        {
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 1) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm))
                    {
                        return false;    
                    }
                }
            }
            return true;
        }
        public Lysosome(int wid, int hei)
        {
            type = 7;
            Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            bool success = false;
            int x, y;
            int counter = 0;
            Body = new Part[0];
            do
            {
                int minX = (Field.gridSizeX - wid) / 2;
                int minY = (Field.gridSizeY - hei) / 2;
                x = 1 + minX + r.Next(wid - Mask.GetLength(1));
                y = 1 + minY + r.Next(hei - Mask.GetLength(0));
                if (checkMask(x, y))
                {
                    for (int i = 0; i < Mask.GetLength(1); i++)
                    {
                        for (int j = 0; j < Mask.GetLength(0); j++)
                        {
                            if (Mask[j, i] == 1)
                            {
                                Array.Resize(ref Body, Body.Length + 1);
                                putPart(x + i, y + i);
                                Field.grid[x + i, y + j].content = this.type;
                                Field.grid[x + i, y + j].org = this;
                            }
                        }
                    }
                    Corner.X = x;
                    Corner.Y = y;
                    Center.X = (Corner.X * Field.cellSizeX + (Mask.GetLength(1) * Field.cellSizeX) / 2);
                    Center.Y = (Corner.Y * Field.cellSizeY + (Mask.GetLength(0) * Field.cellSizeY) / 2);
                    success = true;
                }
                counter++;
            }
            while ((!success) && (counter < 1000000));

            if (counter == 1000000)
            {

                ///////////
                //// error!!!!!!!!!!!!!!!!!!!! �� ������
                ///////////
            }

            InitResources();


        }
        private void InitResources()
        {
            energy += 30;
            resources[(int)Resources.ATP] = 0;
            resources[(int)Resources.oxygen] = 1;
            resources[(int)Resources.aminoacid] = 0;
            resources[(int)Resources.protein] = 0;
            resources[(int)Resources.nucleobase] = 0;
            resources[(int)Resources.RNA] = 0;
            resources[(int)Resources.DNA] = 0;
            resources[(int)Resources.carbohydrate] = 0;
            resources[(int)Resources.monosaccharide] = 1;
            resources[(int)Resources.lipid] = 0;
            resources[(int)Resources.fattyacid] = 0;
            resources[(int)Resources.phosphate] = 0;
            resources[(int)Resources.carbondioxide] = 0;
            resources[(int)Resources.micronutrient] = 0;
            importance[(int)Resources.ATP] = 0;
            importance[(int)Resources.oxygen] = 1;
            importance[(int)Resources.aminoacid] = 0;
            importance[(int)Resources.protein] = 0;
            importance[(int)Resources.nucleobase] = 0;
            importance[(int)Resources.RNA] = 0;
            importance[(int)Resources.DNA] = 0;
            importance[(int)Resources.carbohydrate] = 1;
            importance[(int)Resources.monosaccharide] = 1;
            importance[(int)Resources.lipid] = 0;
            importance[(int)Resources.fattyacid] = 0;
            importance[(int)Resources.phosphate] = 0;
            importance[(int)Resources.carbondioxide] = 0;
            importance[(int)Resources.micronutrient] = 1;
        }

        private enum Actions {nothing, splitCarbohydrate, splitLipid, splitProtein, splitCarbohydrateFast, splitLipidFast, splitProteinFast, coolDown};
        private Actions action = Actions.nothing;
        private void ChooseBehavior()
        {
            if ((energy > 900) || ((energy > 300) && (r.Next(901 - energy) == 0)))
            {
                action = Actions.coolDown;
                busy = 6000;
                return;
            }
            switch (r.Next(1000))
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    if (resources[(int)Resources.carbohydrate] >= 1)
                    {
                        busy = 16000;
                        action = Actions.splitCarbohydrate;
                        resources[(int)Resources.carbohydrate] -= 1;
                        if (resources[(int)Resources.micronutrient] >= 1)
                        {
                            action = Actions.splitCarbohydrateFast;
                            resources[(int)Resources.micronutrient] -= 1;
                            busy = 2000;
                        }
                    }
                    break;
                case 6:
                    if (resources[(int)Resources.lipid] >= 1)
                    {
                        busy = 20000;
                        action = Actions.splitLipid;
                        resources[(int)Resources.lipid] -= 1;
                        if (resources[(int)Resources.micronutrient] >= 1)
                        {
                            action = Actions.splitLipidFast;
                            resources[(int)Resources.micronutrient] -= 1;
                            busy = 2500;
                        }
                    }
                    break;
                case 7:
                    if (resources[(int)Resources.protein] >= 1)
                    {
                        busy = 32000;
                        action = Actions.splitProtein;
                        resources[(int)Resources.protein] -= 1;
                        if (resources[(int)Resources.micronutrient] >= 1)
                        {
                            action = Actions.splitProteinFast;
                            resources[(int)Resources.micronutrient] -= 1;
                            busy = 4000;
                        }
                    }
                    break;
            }
        }

        private void FinishAction()
        {
            switch (action)
            {
                case Actions.splitCarbohydrate:
                case Actions.splitCarbohydrateFast:
                    action = Actions.nothing;
                    resources[(int)Resources.monosaccharide] += 12;
                    break;
                case Actions.splitLipid:
                case Actions.splitLipidFast:
                    action = Actions.nothing;
                    resources[(int)Resources.fattyacid] += 9;
                    break;
                case Actions.splitProtein:
                case Actions.splitProteinFast:
                    action = Actions.nothing;
                    resources[(int)Resources.aminoacid] += 25;
                    break;
                case Actions.coolDown:
                    action = Actions.nothing;
                    break;
            }

        }
        public override void Update()
        {
            time++;
            if (time == maxtime)
            {
                time = 0;
            }


            if (energy > 0)
            {
                if (busy > 0)
                {
                    busy--;
                    switch (action)
                    {
                        case Actions.coolDown:
                            if (time % 64 == 0)
                            {
                                energy--;
                            }
                            break;
                        case Actions.splitCarbohydrateFast:
                            if (time % 32 == 0)
                            {
                                energy++;
                            }
                            break;
                        case Actions.splitLipidFast:
                            if (time % 16 == 0)
                            {
                                energy++;
                            }
                            break;
                        case Actions.splitProteinFast:
                            if (time % 64 == 0)
                            {
                                energy++;
                            }
                            break;
                        case Actions.splitCarbohydrate:
                            if (time % 256 == 0)
                            {
                                energy++;
                            }
                            break;
                        case Actions.splitLipid:
                            if (time % 128 == 0)
                            {
                                energy++;
                            }
                            break;
                        case Actions.splitProtein:
                            if (time % 512 == 0)
                            {
                                energy++;
                            }
                            break;
                    }
                    if (busy == 0)
                    {
                        FinishAction();
                    }
                }
                else
                    ChooseBehavior();
            }


            if (time % 512 == 0)
            {
                if (energy > 0)
                {
                    energy--;
                }
            }
            if (energy == 0)
            {
                if (r.Next(4) > 0)
                {
                    ConsumeATP();
                    if (energy == 0)
                    {
                        ConsumeGlucose();
                    }
                }
                else
                {
                    ConsumeGlucose();
                    if (energy == 0)
                    {
                        ConsumeATP();
                    }
                }

            }
            if (energy == 0)
            {
                ////////////////////
                //// ��������� �������
                //////////////////
            }
        }
    }

    public class Ribosome : Organelle
    {
        private Random r = new Random();
        private Organelle parent;
        private int[,] Mask =
        {
            {1}
        };
        private int delivered;
        private bool checkMask(int x, int y)
        {
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 1) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.EndoplasmicReticulum))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public Ribosome(int wid, int hei, Organelle nucleus)
        {
            type = 8;
            parent = nucleus;
            Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            bool success = false;
            int x, y;
            int counter = 0;
            Body = new Part[0];
            do
            {
                int minX = (Field.gridSizeX - wid) / 2;
                int minY = (Field.gridSizeY - hei) / 2;
                x = 1 + minX + r.Next(wid - Mask.GetLength(1));
                y = 1 + minY + r.Next(hei - Mask.GetLength(0));
                if (checkMask(x, y))
                {
                    for (int i = 0; i < Mask.GetLength(1); i++)
                    {
                        for (int j = 0; j < Mask.GetLength(0); j++)
                        {
                            if (Mask[j, i] == 1)
                            {
                                Array.Resize(ref Body, Body.Length + 1);
                                putPart(x + i, y + i);
                                Field.grid[x + i, y + j].content = this.type;
                                Field.grid[x + i, y + j].org = this;
                            }
                        }
                    }
                    Corner.X = x;
                    Corner.Y = y;
                    Center.X = (Corner.X * Field.cellSizeX + (Mask.GetLength(1) * Field.cellSizeX) / 2);
                    Center.Y = (Corner.Y * Field.cellSizeY + (Mask.GetLength(0) * Field.cellSizeY) / 2);
                    success = true;
                }
                counter++;
            }
            while ((!success) && (counter < 1000000));

            if (counter == 1000000)
            {

                ///////////
                //// error!!!!!!!!!!!!!!!!!!!! �� ������
                ///////////
            }

            InitResources();


        }
        private void InitResources()
        {
            energy += 30;
            resources[(int)Resources.ATP] = 2;
            resources[(int)Resources.oxygen] = 0;
            resources[(int)Resources.aminoacid] = 0;
            resources[(int)Resources.protein] = 0;
            resources[(int)Resources.nucleobase] = 0;
            resources[(int)Resources.RNA] = 1;
            resources[(int)Resources.DNA] = 0;
            resources[(int)Resources.carbohydrate] = 0;
            resources[(int)Resources.monosaccharide] = 0;
            resources[(int)Resources.lipid] = 0;
            resources[(int)Resources.fattyacid] = 0;
            resources[(int)Resources.phosphate] = 0;
            resources[(int)Resources.carbondioxide] = 0;
            resources[(int)Resources.micronutrient] = 0;
            importance[(int)Resources.ATP] = 1;
            importance[(int)Resources.oxygen] = 0;
            importance[(int)Resources.aminoacid] = 0;
            importance[(int)Resources.protein] = 0;
            importance[(int)Resources.nucleobase] = 0;
            importance[(int)Resources.RNA] = 0;
            importance[(int)Resources.DNA] = 0;
            importance[(int)Resources.carbohydrate] = 0;
            importance[(int)Resources.monosaccharide] = 0;
            importance[(int)Resources.lipid] = 0;
            importance[(int)Resources.fattyacid] = 0;
            importance[(int)Resources.phosphate] = 0;
            importance[(int)Resources.carbondioxide] = 0;
            importance[(int)Resources.micronutrient] = 0;
        }

        private enum Actions { nothing, genProtein };
        private Actions action = Actions.nothing;
        private void ChooseBehavior()
        {
            switch (r.Next(1000))
            {
                case 0:
                    if ((parent.resources[(int)Resources.aminoacid] >= 25) && (parent.resources[(int)Resources.RNA] >= 5) && (parent.resources[(int)Resources.RNA] >= 5))
                    {
                        busy = 25000;
                        action = Actions.genProtein;
                        delivered = 0;
                        //resources[(int)Resources.aminoacid] -= 25;
                        //resources[(int)Resources.RNA] -= 5;
                    }
                    break;        
            }
        }

        private void FinishAction()
        {
            switch (action)
            {
                case Actions.genProtein:
                    action = Actions.nothing;
                    resources[(int)Resources.protein] += 1;
                    //send to golgi!
                    break;
            }
        }
        public override void Update()
        {
            time++;
            if (time == maxtime)
            {
                time = 0;
            }


            if (energy > 0)
            {
                if (busy > 0)
                {
                    busy--;
                    switch (action)
                    {
                        case Actions.genProtein:
                            if (time % 256 == 0)
                            {
                                energy--;
                            }
                            if (busy % 1000 == 999)
                            {
                                if ((parent.resources[(int)Resources.aminoacid] >= 1) && (parent.resources[(int)Resources.RNA] >= 1))
                                {
                                    Transport.SendResource((int)Organelle.Resources.aminoacid, 1, parent, this);
                                    Transport.SendResource((int)Organelle.Resources.RNA, 1, parent, this);
                                }
                                else
                                {
                                    busy += 1;
                                }
                            }
                            break;
                    }
                    if (resources[(int)Resources.aminoacid] > 0)
                    {
                        resources[(int)Resources.aminoacid] = 0;
                        delivered++;
                    }
                    if (resources[(int)Resources.RNA] > 1)
                    {
                        Transport.SendResource((int)Organelle.Resources.RNA, 1, this, parent);
                    }
                    if (busy == 0)
                    {
                        if (delivered == 25)
                        {
                            FinishAction();
                        }
                        else
                        {
                            busy = 1;
                        }
                    }
                }
                else
                    ChooseBehavior();
            }


            if (time % 512 == 0)
            {
                if (energy > 0)
                {
                    energy--;
                }
            }
            if (energy == 0)
            {
                if (r.Next(4) > 0)
                {
                    ConsumeATP();
                    if (energy == 0)
                    {
                        ConsumeGlucose();
                    }
                }
                else
                {
                    ConsumeGlucose();
                    if (energy == 0)
                    {
                        ConsumeATP();
                    }
                }

            }
            if (energy == 0)
            {
                ////////////////////
                //// ��������� �������
                //////////////////
            }
        }
    }

    public class GolgiApparatus : Organelle
    {
        private Random r = new Random();
        private int[,] Mask =
        {
            {0,0,2,1,1,1,1,2,0,0},
            {0,1,1,1,1,1,1,1,1,0},
            {2,1,1,1,1,1,1,1,1,2},
            {2,1,1,1,1,1,1,1,1,2},
            {0,1,1,1,1,1,1,1,1,0},
            {0,0,2,1,1,1,1,2,0,0}
        };

        private bool checkMask(int x, int y)
        {
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 1) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm))
                    {
                        return false;
                    }
                    if ((Mask[j, i] == 2) && ((Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm) &&
                        (Field.grid[x + i, y + j].content != (int)Organelle.Types.Nucleus) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.EndoplasmicReticulum)))
                    {
                        return false;
                    }
                }
            }
            //bool b = false;
            for (int i = 0; i < Mask.GetLength(1); i++)
            {
                for (int j = 0; j < Mask.GetLength(0); j++)
                {
                    if ((Mask[j, i] == 2) && (Field.grid[x + i, y + j].content != (int)Organelle.Types.Cytoplasm))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public GolgiApparatus(int wid, int hei)
        {
            type = 9;
            Field.actualReserves[this.type, Field.orgPartialNumber[this.type]].resources = this.resources;
            Field.orgFullNumber++;
            Field.org[this.type, Field.orgPartialNumber[this.type]] = this;
            num = Field.orgPartialNumber[this.type];
            Field.orgPartialNumber[this.type]++;
            bool success = false;
            int x, y;
            int counter = 0;
            Body = new Part[0];
            do
            {
                int minX = (Field.gridSizeX - wid) / 2;
                int minY = (Field.gridSizeY - hei) / 2;
                x = 1 + minX + r.Next(wid - Mask.GetLength(1));
                y = 1 + minY + r.Next(hei - Mask.GetLength(0));
                if (checkMask(x, y))
                {
                    for (int i = 0; i < Mask.GetLength(1); i++)
                    {
                        for (int j = 0; j < Mask.GetLength(0); j++)
                        {
                            if ((Mask[j, i] > 0) && (Field.grid[x + i, y + j].content == (int)Organelle.Types.Cytoplasm))
                            {
                                Array.Resize(ref Body, Body.Length + 1);
                                putPart(x + i, y + i);
                                Field.grid[x + i, y + j].content = this.type;
                                Field.grid[x + i, y + j].org = this;
                            }
                        }
                    }
                    Corner.X = x;
                    Corner.Y = y;
                    Center.X = (Corner.X * Field.cellSizeX + (Mask.GetLength(1) * Field.cellSizeX) / 2);
                    Center.Y = (Corner.Y * Field.cellSizeY + (Mask.GetLength(0) * Field.cellSizeY) / 2);
                    success = true;
                }
                counter++;
            }
            while ((!success) && (counter < 1000000));

            if (counter == 1000000)
            {

                ///////////
                //// error!!!!!!!!!!!!!!!!!!!! �� ������
                ///////////
            }

            InitResources();


        }
        private void InitResources()
        {
            energy += 80;
            resources[(int)Resources.ATP] = 5;
            resources[(int)Resources.oxygen] = 3;
            resources[(int)Resources.aminoacid] = 0;
            resources[(int)Resources.protein] = 0;
            resources[(int)Resources.nucleobase] = 0;
            resources[(int)Resources.RNA] = 0;
            resources[(int)Resources.DNA] = 0;
            resources[(int)Resources.carbohydrate] = 0;
            resources[(int)Resources.monosaccharide] = 3;
            resources[(int)Resources.lipid] = 0;
            resources[(int)Resources.fattyacid] = 0;
            resources[(int)Resources.phosphate] = 0;
            resources[(int)Resources.carbondioxide] = 0;
            resources[(int)Resources.micronutrient] = 0;
            importance[(int)Resources.ATP] = 2;
            importance[(int)Resources.oxygen] = 1;
            importance[(int)Resources.aminoacid] = 1;
            importance[(int)Resources.protein] = 5;
            importance[(int)Resources.nucleobase] = 0;
            importance[(int)Resources.RNA] = 0;
            importance[(int)Resources.DNA] = 0;
            importance[(int)Resources.carbohydrate] = 2;
            importance[(int)Resources.monosaccharide] = 1;
            importance[(int)Resources.lipid] = 2;
            importance[(int)Resources.fattyacid] = 1;
            importance[(int)Resources.phosphate] = 1;
            importance[(int)Resources.carbondioxide] = 1;
            importance[(int)Resources.micronutrient] = 1;
        }

        private enum Actions { nothing };
        //private Actions action = Actions.nothing;
        private void ChooseBehavior()
        {
       /*     if (r.Next(512) == 0)
            {
                if ((resources[(int)Resources.fattyacid] >= 9) && (resources[(int)Resources.micronutrient] >= 1))
                {
                    busy = 25000;
                    action = Actions.genLipid;
                    resources[(int)Resources.fattyacid] -= 9;
                    resources[(int)Resources.micronutrient] -= 1;
                }
            }*/
        }

        private void FinishAction()
        {
      //      switch (action)
      //      {
      //          case Actions.genLipid:
      //              action = Actions.nothing;
      //              resources[(int)Resources.lipid] += 1;
      //              break;
      //      }

        }
        public override void Update()
        {
            time++;
            if (time == maxtime)
            {
                time = 0;
            }


            if (energy > 0)
            {
                if (busy > 0)
                {
                    busy--;
                   // switch (action)
                   // {
                   //     /////////////////////////////
                   // }
                    if (busy == 0)
                    {
                        FinishAction();
                    }
                }
                else
                    ChooseBehavior();
            }


            if (time % 64 == 0)
            {
                if (energy > 0)
                {
                    energy--;
                }
            }
            if (energy == 0)
            {
                if (r.Next(4) > 0)
                {
                    ConsumeATP();
                    if (energy == 0)
                    {
                        ConsumeGlucose();
                    }
                }
                else
                {
                    ConsumeGlucose();
                    if (energy == 0)
                    {
                        ConsumeATP();
                    }
                }

            }
            if (energy == 0)
            {
                ////////////////////
                //// ��������� �������
                ////////////////////
            }
        }
    }

    public static class Transport
    {
        private static Random r = new Random();
        public struct Item
        {
            public Vector2 pos;
            public Vector2 truepos;
            public int timeLeft, tactTime, tact, targetType, targetNumber, resNumber;
            public int type;
            public Vector2 velocity;
            public bool trash;
         //   public object resource;
        }
        public static List<Item> item = new List<Item>();

        public static void SendResource(int res, int resNumber, Organelle Source, Organelle Target)
        {
            Item t = new Item();
            int sourceType = Source.type;
            int sourceNumber = Source.num;
            int targetType = Target.type;
            int targetNumber = Target.num;
            Vector2 source = Field.org[sourceType, sourceNumber].Center;
            Vector2 target = Field.org[targetType, targetNumber].Center;
        //    float x = Field.org[sourceType, sourceNumber].Center.X;
         //   float y = Field.org[minType, minNum].Center.Y; 
            t.type = res;
            t.trash = false;
            if (resNumber < 0)
            {
                resNumber = -resNumber;
                t.trash = true;
            }
            t.pos.X = source.X;
            t.pos.Y = source.Y;
            t.truepos.X = (float)source.X;
            t.truepos.Y = (float)source.Y;
            t.velocity.X = (target.X - source.X) / (float)Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y));
            t.velocity.Y = (target.Y - source.Y) / (float)Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y));
            t.tact = 10;
            t.tactTime = t.tact;
            t.timeLeft = (int)Math.Floor(Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y)));
            t.targetType = targetType;
            t.targetNumber = targetNumber;
            t.resNumber = resNumber;
            item.Add(t);
            Field.expectedReserves[targetType, targetNumber].resources[res] += resNumber;
            Field.actualReserves[sourceType, sourceNumber].resources[res] -= resNumber;
        }
        private static Vector2 outPointTo(Vector2 p)
        {
            Vector2 dir, c, s;
            c.X = Field.screenSizeX / 2;
            c.Y = Field.screenSizeY / 2;
            dir = Vector2.Normalize(p - c);
            s = c + Vector2.Multiply(dir, (float) (1.25 * Math.Sqrt(c.X * c.X + c.Y * c.Y)));
            s.X += r.Next(201) - 100;
            s.Y += r.Next(201) - 100;
            return s;
        }
        public static void OrderResource(int res, int resNumber, int poreNumber)
        {
            Item t = new Item();
            Vector2 source = Transport.outPointTo(Field.org[(int)Organelle.Types.Pore, poreNumber].Center);
            Vector2 target = Field.org[(int)Organelle.Types.Pore, poreNumber].Center;
            //    float x = Field.org[sourceType, sourceNumber].Center.X;
            //   float y = Field.org[minType, minNum].Center.Y; 
            t.type = res;
            t.trash = false;
            t.pos.X = source.X;
            t.pos.Y = source.Y;
            t.truepos.X = (float)source.X;
            t.truepos.Y = (float)source.Y;
            t.velocity.X = (target.X - source.X) / (float)Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y));
            t.velocity.Y = (target.Y - source.Y) / (float)Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y));
            t.tact = 10;
            t.tactTime = t.tact;
            t.timeLeft = (int)Math.Floor(Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y)));
            t.targetType = (int)Organelle.Types.Pore;
            t.targetNumber = poreNumber;
            t.resNumber = resNumber;
            item.Add(t);
            Field.expectedReserves[(int)Organelle.Types.Pore, poreNumber].resources[res] += resNumber;
        }
        private static void ThrowAway(int res, int poreNum)
        {
            Item t = new Item();
            Organelle Source = Field.org[(int)Organelle.Types.Pore, poreNum];
            Vector2 source = Source.Center;
            Vector2 target = outPointTo(Source.Center);
            //    float x = Field.org[sourceType, sourceNumber].Center.X;
            //   float y = Field.org[minType, minNum].Center.Y; 
            t.type = res;
            t.pos.X = source.X;
            t.pos.Y = source.Y;
            t.truepos.X = (float)source.X;
            t.truepos.Y = (float)source.Y;
            t.velocity.X = (target.X - source.X) / (float)Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y));
            t.velocity.Y = (target.Y - source.Y) / (float)Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y));
            t.tact = 10;
            t.tactTime = t.tact;
            t.timeLeft = (int)Math.Floor(Math.Sqrt((target.X - source.X) * (target.X - source.X) + (target.Y - source.Y) * (target.Y - source.Y)));
            item.Add(t);
        }
        public static void Utilize(Organelle Source, int res, int resNum)
        {
            if (Source.type == (int)Organelle.Types.Pore)
            {
                Source.resources[res] -= resNum;
                ThrowAway(res, Source.num);
            }
            else
            {
                SendResource(res, -resNum, Source, Field.org[(int)Organelle.Types.Pore, r.Next(Field.orgPartialNumber[(int)Organelle.Types.Pore])]);
            }
        }
        public static void Update()//(ref Organelle.Resources res, ref int resNumber, ref int targetType, ref int targetNumber)
        {
            Item t;
            for (int i = 0; i < item.Count; i++)
            {

                t = item[i];
                t.tactTime--;
                if (t.tactTime == 0)
                {
                    t.tactTime = t.tact;
                    t.timeLeft--;
                    t.truepos.X += t.velocity.X;
                    t.truepos.Y += t.velocity.Y;
                    t.pos.X = (float)Math.Round(t.truepos.X);
                    t.pos.Y = (float)Math.Round(t.truepos.Y);
                }
                if (t.timeLeft == 0)
                {
                    if ((t.truepos.X < 0) || (t.truepos.X >= Field.screenSizeX) || (t.truepos.Y < 0) || (t.truepos.Y >= Field.screenSizeY))
                    {

                    }
                    else
                    {
                        Field.actualReserves[t.targetType, t.targetNumber].resources[t.type] += t.resNumber;
                        Field.expectedReserves[t.targetType, t.targetNumber].resources[t.type] -= t.resNumber;
                        //   targetType = t.targetType;
                        //  targetNumber = t.targetNumber;
                        //  res = t.type;
                        //  resNumber = t.resNumber;
                        if (t.trash)
                        {
                            Field.actualReserves[t.targetType, t.targetNumber].resources[t.type] -= t.resNumber;
                            ThrowAway(t.type, t.targetNumber);
                        }
                    }
                    item.RemoveAt(i);
                    i--;
                    
                }
                else
                {
                    item[i] = t;
                }
            }
        }
        public static void CalculateNeeds()
        {
            for (int i = 3; i < Field.organelleTypeNumber; i++)
            {
                for (int j = 0; j < Field.orgPartialNumber[i]; j++)
                {
                    Field.org[i, j].CalculateNeeds();
                }
            }
        }
    }

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;
        int width = 35;
        int height = 30;
        int[] orgnum = new int[Field.organelleMaxNumber];
        public struct trigger 
        {
            public int org_type;
            public int org_num;
            public int res_type;
            public int res_threshold;
            public string message_text;
        }

        trigger sms_trigger;
        string password;
        string hostname;
        string login;
        string password_host;
        string path;

        bool exit_mark = false;

        public void wait_for_configuration()
        {
            //string cur_path = Environment.CurrentDirectory + "\\config.txt";
            string cur_path = "config.txt";
    //        if (System.IO.File.Exists(path))
    //        {
    //            System.IO.File.Delete(path);
     //       }
            try
            {
                ConnectionInfo connectionInfo = new PasswordConnectionInfo(hostname, login, password_host);
                using (var scp = new ScpClient(connectionInfo))
                {
                    scp.Connect();
                    //				scp.Downloading += delegate(object sender, ScpDownloadEventArgs e)
                    //				{
                    //					Console.WriteLine(e.Filename + ":" + e.Size + ":" + e.Downloaded);
                    //				};

                    //				scp.Uploading += delegate(object sender, ScpUploadEventArgs e)
                    //		{
                    //				Console.WriteLine(e.Filename + ":" + e.Size + ":" + e.Uploaded);
                    //	};

                    if (System.IO.File.Exists(cur_path))
                    {
                        System.IO.File.Delete(cur_path);
                    }
                    scp.Download(path, new FileInfo(cur_path));
                    scp.Disconnect();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
            
        public Game1(string[] args)
        {
            if (args.Length < 5)
            {
                System.Console.WriteLine("Please enter all needed info in the command line!");
                
                Exit();
                exit_mark = true;
                return;

             //   password = "3edc3edc";
             //   hostname = "balitsky.vdi.mipt.ru";
             //   login = "root";
             //   password_host = "VvmpGd";
             //   path = "config.txt";
                
            }
            password = args[0];
            hostname = args[1];
            login = args[2];
            password_host = args[3];
            path = args[4];
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = Field.screenSizeX;
            graphics.PreferredBackBufferHeight = Field.screenSizeY;
            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            wait_for_configuration();

            char[] delimiterChars = { ' ', '.', '[', ']' };
            string[] lines = System.IO.File.ReadAllLines(@"config.txt");

            sms_trigger.message_text = "Trigger worked but message wasn't set.";
            //System.Console.WriteLine("Contents of WriteLines2.txt = ");
            foreach (string line in lines)
            {
                string[] words = line.Split(delimiterChars);
                switch (words[0])
                {
                    case "Speed":
                        TargetElapsedTime = TimeSpan.FromSeconds(1.0f / (float) (50 * Convert.ToInt16(words[2])));
                        break;
                    case "Width":
                        width = Convert.ToInt16(words[2]);
                        break;
                    case "Height":
                        height = Convert.ToInt16(words[2]);
                        break;
                    case "Nucleus":
                        orgnum[(int)Organelle.Types.Nucleus] = Convert.ToInt16(words[2]);
                        break;
                    case "Mitochondrion":
                        orgnum[(int)Organelle.Types.Mitochondrion] = Convert.ToInt16(words[2]);
                        break;
                    case "Pore":
                        orgnum[(int)Organelle.Types.Pore] = Convert.ToInt16(words[2]);
                        break;
                    case "EndoplasmicReticulum":
                        orgnum[(int)Organelle.Types.EndoplasmicReticulum] = Convert.ToInt16(words[2]);
                        break;
                    case "Lysosome":
                        orgnum[(int)Organelle.Types.Lysosome] = Convert.ToInt16(words[2]);
                        break;
                    case "Ribosome":
                        orgnum[(int)Organelle.Types.Ribosome] = Convert.ToInt16(words[2]);
                        break;
                    case "GolgiApparatus":
                        orgnum[(int)Organelle.Types.GolgiApparatus] = Convert.ToInt16(words[2]);
                        break;
                    case "Trigger":
                        /////
                        switch (words[2])
                        {
                            case "Nucleus":
                                sms_trigger.org_type = (int)Organelle.Types.Nucleus;
                                break;
                            case "Mitochondrion":
                                sms_trigger.org_type = (int)Organelle.Types.Mitochondrion;
                                break;
                            case "EndoplasmicReticulum":
                                sms_trigger.org_type = (int)Organelle.Types.EndoplasmicReticulum;
                                break;
                            case "Lysosome":
                                sms_trigger.org_type = (int)Organelle.Types.Lysosome;
                                break;
                            case "Ribosome":
                                sms_trigger.org_type = (int)Organelle.Types.Ribosome;
                                break;
                            case "GolgiApparatus":
                                sms_trigger.org_type = (int)Organelle.Types.GolgiApparatus;
                                break;
                        }

                        switch (words[2])
                        {
                            case "Nucleus":
                                sms_trigger.org_type = (int)Organelle.Types.Nucleus;
                                break;
                            case "Mitochondrion":
                                sms_trigger.org_type = (int)Organelle.Types.Mitochondrion;
                                break;
                            case "EndoplasmicReticulum":
                                sms_trigger.org_type = (int)Organelle.Types.EndoplasmicReticulum;
                                break;
                            case "Lysosome":
                                sms_trigger.org_type = (int)Organelle.Types.Lysosome;
                                break;
                            case "Ribosome":
                                sms_trigger.org_type = (int)Organelle.Types.Ribosome;
                                break;
                            case "GolgiApparatus":
                                sms_trigger.org_type = (int)Organelle.Types.GolgiApparatus;
                                break;
                        }

                        switch (words[5])
                        {
                            case "ATPs":
                                sms_trigger.res_type = (int)Organelle.Resources.ATP;
                                break;
                            case "oxygen":
                                sms_trigger.res_type = (int)Organelle.Resources.oxygen;
                                break;
                            case "nucleobases":
                                sms_trigger.res_type = (int)Organelle.Resources.nucleobase;
                                break;
                            case "carbondioxide":
                                sms_trigger.res_type = (int)Organelle.Resources.carbondioxide;
                                break;
                            case "aminoacids":
                                sms_trigger.res_type = (int)Organelle.Resources.aminoacid;
                                break;
                            case "monosaccharides":
                                sms_trigger.res_type = (int)Organelle.Resources.monosaccharide;
                                break;
                            case "fattyacids":
                                sms_trigger.res_type = (int)Organelle.Resources.fattyacid;
                                break;
                            case "RNAs":
                                sms_trigger.res_type = (int)Organelle.Resources.RNA;
                                break;
                            case "phosphates":
                                sms_trigger.res_type = (int)Organelle.Resources.phosphate;
                                break;
                            case "proteins":
                                sms_trigger.res_type = (int)Organelle.Resources.protein;
                                break;
                            case "carbohydrates":
                                sms_trigger.res_type = (int)Organelle.Resources.carbohydrate;
                                break;
                            case "lipids":
                                sms_trigger.res_type = (int)Organelle.Resources.lipid;
                                break;
                            case "DNAs":
                                sms_trigger.res_type = (int)Organelle.Resources.DNA;
                                break;
                            case "micronutrients":
                                sms_trigger.res_type = (int)Organelle.Resources.micronutrient;
                                break; 
                        }
                        
                        sms_trigger.org_num = Convert.ToInt16(words[3]);
                        sms_trigger.res_threshold = Convert.ToInt16(words[7]);

                        /////
                        break;
                    case "Message":
                        sms_trigger.message_text = line.Remove(0, 10);
                        break;
                }
            }

        }

     //   Field field = new Field();
        

        Membrane membrane;// = new Membrane(width, height);
        Cytoplasm cytoplasm;// = new Cytoplasm();
        Nucleus[] nucleus = new Nucleus[0]; // = new Nucleus(width, height);
        Mitochondrion[] mitochondrion = new Mitochondrion[0];
        Pore[] pore = new Pore[0];
        EndoplasmicReticulum[] endoplasmicReticulum = new EndoplasmicReticulum[0];
        Lysosome[] lysosome = new Lysosome[0];
        Ribosome[] ribosome = new Ribosome[0];
        GolgiApparatus[] golgiApparatus = new GolgiApparatus[0];
        Random R = new Random();
        protected override void Initialize()
        {
            
          //  TargetElapsedTime = TimeSpan.FromSeconds(1.0f / 700.0f);

           // Field.emptyField();
            clickMB.content = -1;

            membrane = new Membrane(width, height);
            cytoplasm = new Cytoplasm();

            for (int i = 0; i < orgnum[(int)Organelle.Types.Nucleus]; i++)
            {
                Array.Resize(ref nucleus, nucleus.Length + 1);
                nucleus[i] = new Nucleus(width, height);
            }
            for (int i = 0; i < orgnum[(int)Organelle.Types.Pore]; i++)
            {
                Array.Resize(ref pore, pore.Length + 1);
                pore[i] = new Pore();
            }
            for (int i = 0; i < orgnum[(int)Organelle.Types.EndoplasmicReticulum]; i++)
            {    
                Array.Resize(ref endoplasmicReticulum, endoplasmicReticulum.Length + 1);
                endoplasmicReticulum[i] = new EndoplasmicReticulum(width, height);
            }
            for (int i = 0; i < orgnum[(int)Organelle.Types.GolgiApparatus]; i++)
            {
                Array.Resize(ref golgiApparatus, golgiApparatus.Length + 1);
                golgiApparatus[i] = new GolgiApparatus(width, height);
            }
            for (int i = 0; i < orgnum[(int)Organelle.Types.Mitochondrion]; i++)
            {
                Array.Resize(ref mitochondrion, mitochondrion.Length + 1);
                mitochondrion[i] = new Mitochondrion(width, height);
            }
            for (int i = 0; i < orgnum[(int)Organelle.Types.Lysosome]; i++)
            {
                Array.Resize(ref lysosome, lysosome.Length + 1);
                lysosome[i] = new Lysosome(width, height);
            }
            for (int i = 0; i < orgnum[(int)Organelle.Types.Ribosome]; i++)
            {
                Array.Resize(ref ribosome, ribosome.Length + 1);
                ribosome[i] = new Ribosome(width, height, nucleus[R.Next(Field.orgPartialNumber[(int)Organelle.Types.Nucleus])]);
            }
            

            base.Initialize();
        }

        Texture2D[] membraneDDS = new Texture2D[16];
        Texture2D cytoplasmDDS;
        Texture2D[] orgDDS = new Texture2D[Field.organelleTypeNumber];
        Texture2D[] resDDS = new Texture2D[Field.resourceTypeNumber];
        
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("Courier New");
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (i != j)
                    {
                        membraneDDS[4 * i + j] = Content.Load<Texture2D>("mbr" + Convert.ToString(i) + Convert.ToString(j));
                    }
                }
            }
            cytoplasmDDS = Content.Load<Texture2D>("cpl");
            orgDDS[(int)Organelle.Types.Nucleus] = Content.Load<Texture2D>("ncl");
            orgDDS[(int)Organelle.Types.Mitochondrion] = Content.Load<Texture2D>("mth3l");
            orgDDS[(int)Organelle.Types.Pore] = Content.Load<Texture2D>("pore");
            orgDDS[(int)Organelle.Types.EndoplasmicReticulum] = Content.Load<Texture2D>("eps");
            orgDDS[(int)Organelle.Types.Lysosome] = Content.Load<Texture2D>("liz");
            orgDDS[(int)Organelle.Types.Ribosome] = Content.Load<Texture2D>("rib");
            orgDDS[(int)Organelle.Types.GolgiApparatus] = Content.Load<Texture2D>("gol");

            resDDS[(int)Organelle.Resources.ATP] = Content.Load<Texture2D>("atp");
            resDDS[(int)Organelle.Resources.oxygen] = Content.Load<Texture2D>("oxy");
            resDDS[(int)Organelle.Resources.aminoacid] = Content.Load<Texture2D>("ami");
            resDDS[(int)Organelle.Resources.fattyacid] = Content.Load<Texture2D>("fat");
            resDDS[(int)Organelle.Resources.lipid] = Content.Load<Texture2D>("lip");
            resDDS[(int)Organelle.Resources.carbohydrate] = Content.Load<Texture2D>("car");
            resDDS[(int)Organelle.Resources.carbondioxide] = Content.Load<Texture2D>("co2");
            resDDS[(int)Organelle.Resources.monosaccharide] = Content.Load<Texture2D>("mon");
            resDDS[(int)Organelle.Resources.phosphate] = Content.Load<Texture2D>("pho");
            resDDS[(int)Organelle.Resources.micronutrient] = Content.Load<Texture2D>("mic");
            resDDS[(int)Organelle.Resources.nucleobase] = Content.Load<Texture2D>("nuc");
            resDDS[(int)Organelle.Resources.protein] = Content.Load<Texture2D>("pro");
            resDDS[(int)Organelle.Resources.RNA] = Content.Load<Texture2D>("rna");
            resDDS[(int)Organelle.Resources.DNA] = Content.Load<Texture2D>("dna");
        }
        
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        ButtonState leftMB = ButtonState.Released;
        Field.gridCell clickMB = new Field.gridCell();
        bool sent_mail = false;

        bool triggered(trigger t)
        {
            if (Field.org[t.org_type, t.org_num].resources[t.res_type] >= t.res_threshold)
            {
                return true;
            }
            return false;
        }

        protected override void Update(GameTime gameTime)
        {
           // int tT = 0, tN = 0, rN = 0;
           // Organelle.Resources res = 0;
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
            if ((leftMB == ButtonState.Pressed) && (Mouse.GetState().LeftButton == ButtonState.Released))
            {
                int xMouse = Mouse.GetState().X / Field.cellSizeX;
                int yMouse = Mouse.GetState().Y / Field.cellSizeY;
                if ((xMouse < Field.gridSizeX) && (xMouse >= 0) && (yMouse < Field.gridSizeY) && (yMouse >= 0))
                {
                    clickMB = Field.grid[xMouse, yMouse];
                }
            }
            leftMB = Mouse.GetState().LeftButton;



            for (int i = 3; i < Field.organelleTypeNumber; i++)
            {
                for (int j = 0; j < Field.orgPartialNumber[i]; j++)
                {
                    Field.org[i,j].Update();
                }
            }


            Transport.CalculateNeeds();
            
            
            for (int k = 0; k < Field.resourceTypeNumber; k++)
            {
                int maxSurplus = -1000;
                int maxNeed = -1000;
                int nType = 0, nNum = 0, sType = 0, sNum = 0, r1 = R.Next(100), r2;
                for (int i = 3; i < Field.organelleTypeNumber; i++)
                {
                    for (int j = 0; j < Field.orgPartialNumber[i]; j++)
                    {
                        if (Field.surpluses[i, j].resources[k] > maxSurplus)
                        {
                            maxSurplus = Field.surpluses[i, j].resources[k];
                            sType = i;
                            sNum = j;
                        }
                        if (Field.surpluses[i, j].resources[k] == maxSurplus)
                        {
                            r2 = R.Next(100);
                            if (r2 > r1)
                            {
                                maxSurplus = Field.surpluses[i, j].resources[k];
                                sType = i;
                                sNum = j;
                            }
                            r1 = r2;
                        }
                        if (Field.needs[i, j].resources[k] > maxNeed)
                        {
                            maxNeed = Field.needs[i, j].resources[k];
                            nType = i;
                            nNum = j;
                        }
                        if (Field.needs[i, j].resources[k] == maxNeed)
                        {
                            r2 = R.Next(100);
                            if (r2 > r1)
                            {
                                maxNeed = Field.needs[i, j].resources[k];
                                nType = i;
                                nNum = j;
                            }
                            r1 = r2;
                        }
                    }
                }
                if ((maxSurplus + maxNeed > 40) && (maxNeed > -1000) && (maxSurplus > -1000) && ((nType != sType) || (nNum != sNum)))
                {
                    Transport.SendResource(k, 1, Field.org[sType, sNum], Field.org[nType, nNum]);
                }
            }
            Transport.Update();//(ref res, ref rN, ref tT, ref tN);
            if (exit_mark)
            {
                return;
            }
            
            if ((triggered(sms_trigger)) && (!sent_mail))
            {
                MailMessage message;
                SmtpClient client;
                message = new System.Net.Mail.MailMessage(
                    "cell.project.network@gmail.com",
                    "syhyzyre@sms.ru",
                    "Message from the CELL",
                    sms_trigger.message_text);

                client = new SmtpClient("smtp.gmail.com", 25)
                {
                    Credentials = new NetworkCredential("cell.project.network@gmail.com", password),
                    EnableSsl = true
                };
 
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;

                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.ToString());
                }
 
                sent_mail = true;
            }
            base.Update(gameTime);
        }

        Vector2 drawPos, fontPos;
        string output;
        string[] orgName = {"Intercellular space", "Membrane", "Cytoplasm", "Nucleus", "Mitochondrion", "Transport proteins", 
                               "Endoplasmic Reticulum", "Lysosome", "Ribosomes", "Golgi apparatus"};
        float[] orgScale = { 1, 1, 1, 112f / 320f, 64f / 160f, 1, 
                               1f/10f, 1f/10f, 1, 1f/10f };
        float[] drawPriority = {0, 0, 0, 1, 0, 0, 0, 0, 1, 1};
        protected override void Draw(GameTime gameTime)
        {
            //BlendState.AlphaBlend
            GraphicsDevice.Clear(Color.Bisque);
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            for (int i = 0; i < membrane.partNumber; i++)
            {
                drawPos.X = membrane.Body[i].X * Field.cellSizeX;
                drawPos.Y = membrane.Body[i].Y * Field.cellSizeX;
                spriteBatch.Draw(membraneDDS[membrane.PartOrientation(i)], drawPos, Color.White);
            }

            for (int i = 0; i < cytoplasm.partNumber; i++)
            {
                drawPos.X = cytoplasm.Body[i].X * Field.cellSizeX;
                drawPos.Y = cytoplasm.Body[i].Y * Field.cellSizeX;
                spriteBatch.Draw(cytoplasmDDS, drawPos, Color.White);
            }
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            for (int i = 0; i < Transport.item.Count; i++)
            {
                spriteBatch.Draw(resDDS[Transport.item[i].type], Transport.item[i].pos, Color.White);
            }
            
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            for (int k = 3; k < Field.organelleTypeNumber; k++)
            {
                for (int i = 0; i < Field.orgPartialNumber[k]; i++)
                {
                    drawPos.X = Field.org[k, i].Corner.X * Field.cellSizeX;
                    drawPos.Y = Field.org[k, i].Corner.Y * Field.cellSizeX;
                    spriteBatch.Draw(orgDDS[k], drawPos, null, Color.White, 0, Vector2.Zero, orgScale[k], SpriteEffects.None, drawPriority[k]);  
                }
            }
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            if (clickMB.content == -1)
            {
                fontPos.X = 300;
                fontPos.Y = 3;
                output = "Welcome to the Cell";
                spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, 1, SpriteEffects.None, 0);

                fontPos.X = 585;
                fontPos.Y = 2;
                output = "Cell";
                spriteBatch.DrawString(spriteFont, output, fontPos, Color.DarkCyan, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
            }
            else
            {
                fontPos.X = 3;
                fontPos.Y = 3;
                output = orgName[clickMB.content];
                spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.6, SpriteEffects.None, 0);
            }

            switch (clickMB.content)
            {
                case (int)Organelle.Types.Nucleus:
                case (int)Organelle.Types.Mitochondrion:
                case (int)Organelle.Types.EndoplasmicReticulum:
                case (int)Organelle.Types.Lysosome:
                case (int)Organelle.Types.Ribosome:
                case (int)Organelle.Types.GolgiApparatus:
                    fontPos.X = 455;
                    fontPos.Y = 3;
                    output = Convert.ToString(clickMB.org.energy);
                    if (clickMB.org.energy > 0)
                    {
                        spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    }
                    else
                    {
                        spriteBatch.DrawString(spriteFont, output, fontPos, Color.Red, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    }
                    fontPos.X = 490;
                    fontPos.Y = 3;
                    output = "energy";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);

                    fontPos.X = 455;
                    fontPos.Y = 23;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.ATP]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 490;
                    fontPos.Y = 23;
                    output = "ATPs";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 448;
                    drawPos.Y = 30;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.ATP], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 455;
                    fontPos.Y = 43;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.oxygen]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 490;
                    fontPos.Y = 43;
                    output = "oxygen";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 448;
                    drawPos.Y = 50;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.oxygen], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 455;
                    fontPos.Y = 63;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.nucleobase]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 490;
                    fontPos.Y = 63;
                    output = "nucleobases";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 448;
                    drawPos.Y = 70;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.nucleobase], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 455;
                    fontPos.Y = 83;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.carbondioxide]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 490;
                    fontPos.Y = 83;
                    output = "carbon dioxide";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 448;
                    drawPos.Y = 90;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.carbondioxide], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    

                    fontPos.X = 650;
                    fontPos.Y = 3;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.aminoacid]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 685;
                    fontPos.Y = 3;
                    output = "amino acids";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 643;
                    drawPos.Y = 10;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.aminoacid], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 650;
                    fontPos.Y = 23;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.monosaccharide]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 685;
                    fontPos.Y = 23;
                    output = "monosaccharides";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 643;
                    drawPos.Y = 30;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.monosaccharide], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 650;
                    fontPos.Y = 43;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.fattyacid]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 685;
                    fontPos.Y = 43;
                    output = "fatty acids";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 643;
                    drawPos.Y = 50;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.fattyacid], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 650;
                    fontPos.Y = 63;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.RNA]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 685;
                    fontPos.Y = 63;
                    output = "RNAs";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 641;
                    drawPos.Y = 70;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.RNA], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 650;
                    fontPos.Y = 83;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.phosphate]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 685;
                    fontPos.Y = 83;
                    output = "phosphates";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 643;
                    drawPos.Y = 90;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.phosphate], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 845;
                    fontPos.Y = 3;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.protein]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 880;
                    fontPos.Y = 3;
                    output = "proteins";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 838;
                    drawPos.Y = 10;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.protein], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 845;
                    fontPos.Y = 23;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.carbohydrate]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 880;
                    fontPos.Y = 23;
                    output = "carbohydrates";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 838;
                    drawPos.Y = 30;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.carbohydrate], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 845;
                    fontPos.Y = 43;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.lipid]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 880;
                    fontPos.Y = 43;
                    output = "lipids";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 838;
                    drawPos.Y = 50;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.lipid], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 845;
                    fontPos.Y = 63;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.DNA]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 880;
                    fontPos.Y = 63;
                    output = "DNAs";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 836;
                    drawPos.Y = 70;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.DNA], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                    
                    fontPos.X = 845;
                    fontPos.Y = 83;
                    output = Convert.ToString(clickMB.org.resources[(int)Organelle.Resources.micronutrient]);
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    fontPos.X = 880;
                    fontPos.Y = 83;
                    output = "micronutrients";
                    spriteBatch.DrawString(spriteFont, output, fontPos, Color.Black, 0, Vector2.Zero, (float)0.5, SpriteEffects.None, 1);
                    drawPos.X = 838;
                    drawPos.Y = 90;
                    spriteBatch.Draw(resDDS[(int)Organelle.Resources.micronutrient], drawPos, null, Color.White, 0, Vector2.Zero, 2, SpriteEffects.None, 1);
                   
                    break;
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
